const std = @import("std");

inline fn R(
    comptime mul:   comptime_int,
    comptime shift: comptime_int,
    x: *isize,
    y: *isize,
) void {
    var __ = x.*;
    x.* -= mul * y.* >> shift;
    y.* += mul * __ >> shift;
    __   = 3145728 - x.* * x.* - y.* * y.* >> 11;
    x.*  = x.* * __ >> 10;
    y.*  = y.* * __ >> 10;
}

pub export fn _start() noreturn { // callconv(.Naked)
    defer std.os.exit(0);

    const stdout = comptime std.io.getStdOut();

    var b: [1760]u8 = undefined; // Text buffer.
    var z: [1760]i8 = undefined; // Z buffer.

    const R1 = 1;
    const R2 = 2048;
    const K2 = 5120 * 1024;

    var sA: isize = 1024;
    var cA: isize = 0;
    var sB: isize = 1024;
    var cB: isize = 0;

    while (true) {
        b = (.{ ' ' } ** 79 ++ .{ '\n' }) ** 22;
        z = .{ 127 } ** 1760; // This should be the same as @memset().

        var sj: isize = 0;
        var cj: isize = 1024;

        var j: usize = 0;
        while (j < 90) : (j += 1) {
            var si: isize = 0;    // Sine of angle i.
            var ci: isize = 1024; // Cosine of angle i.

            var i: usize = 0;
            while (i < 324) : (i += 1) {
                const x0 = R1 * cj + R2;
                const x1 = ci * x0 >> 10;
                const x2 = cA * sj >> 10;
                const x3 = si * x0 >> 10;
                const x4 = R1 * x2 - (sA * x3 >> 10);
                const x5 = sA * sj >> 10;
                const x6 = K2 + R1 * 1024 * x5 + cA * x3;
                const x7 = cj * si >> 10;
                const x  = 40 + @divFloor(30 * (cB * x1 - sB * x4), x6);
                const y  = 12 + @divFloor(15 * (cB * x4 + sB * x1), x6);

                const o  = @intCast(usize, x + 80 * y);
                const zz = @intCast(i8, (x6 - K2) >> 15);
                if (x < 80 and y < 22 and zz < z[o]) {
                    z[o] = zz;
                    b[o] = ".,-~:;=!*#$@"[
                        @intCast(usize, @max((-cA * x7 - cB * ((-sA * x7 >> 10) + x2) -
                            ci * (cj * sB >> 10) >> 10) - x5 >> 7, 0))
                    ];
                }
                R(5, 8, &ci, &si); // Rotate i.
            }
            R(9, 7, &cj, &sj); // Rotate j.
        }
        R(5, 7, &cA, &sA);
        R(5, 8, &cB, &sB);

        _ = stdout.write(&b) catch return;
        std.time.sleep(15_000_000);
        _ = stdout.write("\x1B[22A") catch return;
    }
}
