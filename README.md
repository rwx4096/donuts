# THE spinning ASCII donut ported to Zig

- My version in Zig: ```donut.zig```
- Optimized version in C: ```donut.c```, [a1k0n.net/2021/01/13/optimizing-donut.html](https://www.a1k0n.net/2021/01/13/optimizing-donut.html)
- Original version in C: [a1k0n.net/2011/07/20/donut-math.html](https://www.a1k0n.net/2011/07/20/donut-math.html)
- Donuts.. mmm yummers

## Building

- Clone the repository: ```git clone https://gitlab.com/rwx4096/donuts.git```
- Run the build script: ```./build zig``` (For more info, run: ```./build```)
